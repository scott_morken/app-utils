<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/8/15
 * Time: 1:19 PM
 */

namespace Smorken\Utils\Hash;


abstract class Base {

    protected $salt = '';

    public function __construct($salt = '')
    {
        $this->salt = $salt;
    }

}