<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/8/15
 * Time: 1:18 PM
 */

namespace Smorken\Utils\Hash\Contract;


interface Hash {

    public function hash($string);
}