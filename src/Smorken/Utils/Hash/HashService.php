<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/8/15
 * Time: 1:16 PM
 */

namespace Smorken\Utils\Hash;

use Smorken\Service\Service;

class HashService extends Service {

    public function start()
    {
        $this->name = 'hash';
    }

    public function load()
    {
        $app = $this->app;
        $this->app->instance($this->getName(), function($c) use ($app) {
            $salt = $app['config']->get('hash.salt', '');
            return new Md5Hash($salt);
        });
    }

}