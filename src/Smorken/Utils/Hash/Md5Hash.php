<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/8/15
 * Time: 1:19 PM
 */

namespace Smorken\Utils\Hash;


use Smorken\Utils\Hash\Contract\Hash;

class Md5Hash extends Base implements Hash {

    public function hash($string)
    {
        $string .= $this->salt;
        return md5($string);
    }
}