<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 10:08 AM
 */

namespace Smorken\Utils;


class PathUtils {

    /**
     * Returns 'base' from the paths loaded into app
     * @return mixed
     */
    public static function base()
    {
        return static::get('base');
    }

    /**
     * Gets a path by key name
     * //todo remove dependency on App
     * @param $key
     * @return mixed
     */
    public static function get($key)
    {
        $app = \Smorken\Application\App::getInstance();
        $fullkey = 'path.' . $key;
        return $app[$fullkey];
    }
} 