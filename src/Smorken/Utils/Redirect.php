<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 2:13 PM
 */

namespace Smorken\Utils;


class Redirect {

    /**
     * Simple redirector, removes ignore from url before redirecting
     * @param $url
     * @param null $ignore
     */
    public static function go($url, $ignore = null)
    {
        if ($ignore !== null) {
            $url = Url::removeFromQueryString($url, $ignore);
        }
        header('Location: ' . $url);
        exit;
    }
} 