<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/25/14
 * Time: 6:56 AM
 */

namespace Smorken\Utils;


class StrUtils {

    /**
     * Checks if string $haystack starts with $needle
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function startsWith($haystack, $needle)
    {
        if (is_array($needle)) {
            foreach($needle as $n) {
                if (static::startsWith($haystack, $n)) {
                    return true;
                }
            }
        }
        else {
            return $needle === '' || strpos($haystack, $needle) === 0;
        }
        return false;
    }

    /**
     * Checks if string $haystack ends with $needle
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function endsWith($haystack, $needle)
    {
        if (is_array($needle)) {
            foreach($needle as $n) {
                if (static::endsWith($haystack, $n)) {
                    return true;
                }
            }
        }
        else {
            return $needle === '' || substr($haystack, -strlen($needle)) === $needle;
        }
        return false;
    }
} 