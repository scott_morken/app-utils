<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 2/6/14
 * Time: 9:35 AM
 */

namespace Smorken\Utils;


class Url {

    /**
     * Creates a full url from $s ($_SERVER)
     * @param null|array $s
     * @param bool $secure
     * @return string
     */
    public static function full($s = null, $secure = false)
    {
        if ($s === null) {
            $s = $_SERVER;
        }
        $ssl = self::isSecure($s);
        $sp = strtolower($s['SERVER_PROTOCOL']);
        $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
        $port = $s['SERVER_PORT'];
        $port = ((!$ssl && $port === '80') || ($ssl && $port === '443')) ? '' : ':'.$port;
        if (isset($s['HTTP_X_FORWARDED_SERVER'])) {
            $host = $s['HTTP_X_FORWARDED_SERVER'];
        } elseif (isset($s['HTTP_X_FORWARDED_HOST'])) {
            $host = $s['HTTP_X_FORWARDED_HOST'];
        } elseif (isset($s['SERVER_NAME'])) {
            $host = $s['SERVER_NAME'];
        }
        else {
            $host = $s['HTTP_HOST'];
        }
        $port = stripos($host, ':') === false ? $port : '';
        if ($secure) {
            $protocol = 'https';
            $port = '';
        }
        return get_purifier()->purify($protocol . '://' . $host . $port . $s['REQUEST_URI']);
    }

    /**
     * Checks if $s ($_SERVER) is an https request
     * @param null|array $s
     * @return bool
     */
    public static function isSecure($s = null)
    {
        if ($s === null) {
            $s = $_SERVER;
        }
        return ((isset($s['HTTPS']) && !empty($s['HTTPS']) && $s['HTTPS'] === 'on') || ($s['SERVER_PORT'] == '443')) ? true:false;
    }

    /**
     * Strips $key from $url if it exists
     * @param $url
     * @param $key
     * @return mixed|string
     */
    public static function removeFromQueryString($url, $key)
    {
        $url = preg_replace('/(?:&|(\?))' . $key . '[=]*[^&]*(?(1)&|)?/i', "$1", $url);
        if (substr($url, -1) === '?') {
            $url = substr($url, 0, -1);
        }
        return $url;
    }

    public static function replaceInQueryString($url, $key, $value)
    {
        $expUrl = explode('?', $url);
        $qs = array();
        if (count($expUrl) > 1) {
            parse_str($expUrl[1], $qs);
        }
        $qs[$key] = $value;
        return $expUrl[0] . '?' . http_build_query($qs);
    }
}
