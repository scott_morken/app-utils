<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 8/25/14
 * Time: 6:59 AM
 */

use Smorken\Utils\StrUtils;

class StrUtilsTest extends \PHPUnit_Framework_TestCase {

    public function testSimpleStartWithTrue()
    {
        $h = 'this is a test';
        $n = 'th';
        $this->assertTrue(StrUtils::startsWith($h, $n));
    }

    public function testSimpleStartWithFalse()
    {
        $h = 'this is a test';
        $n = 'is';
        $this->assertFalse(StrUtils::startsWith($h, $n));
    }

    public function testArrayStartWithTrue()
    {
        $h = 'this is a test';
        $n = array('a', 'fiz', 'th', 'est');
        $this->assertTrue(StrUtils::startsWith($h, $n));
    }

    public function testArrayStartWithFalse()
    {
        $h = 'this is a test';
        $n = array('a', 'fiz', 'his', 'est');
        $this->assertFalse(StrUtils::startsWith($h, $n));
    }

    public function testSimpleEndWithTrue()
    {
        $h = 'this is a test';
        $n = 'a test';
        $this->assertTrue(StrUtils::endsWith($h, $n));
    }

    public function testSimpleEndWithFalse()
    {
        $h = 'this is a test';
        $n = 'th';
        $this->assertFalse(StrUtils::endsWith($h, $n));
    }

    public function testArrayEndWithTrue()
    {
        $h = 'this is a test';
        $n = array('a', 'fiz', 'th', 'est');
        $this->assertTrue(StrUtils::endsWith($h, $n));
    }

    public function testArrayEndWithFalse()
    {
        $h = 'this is a test';
        $n = array('a', 'fiz', 'his', 'tse');
        $this->assertFalse(StrUtils::endsWith($h, $n));
    }
} 